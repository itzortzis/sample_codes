/*
 * File: encoding.c
 * Author: YianisTz
 * Description: Wall page mechanics for online store
 * #Typescript #Firebase #Json #Pagination
 *
 * Created on May 10, 2019
 */

import { Component, OnInit } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { OnDestroy } from '@angular/core';
import { NavController } from '@ionic/angular';

import { NavParamsService } from '../../services/navParams.service';
import * as firebase from 'firebase';

@Component({
  selector: 'app-be-inspired',
  templateUrl: './be-inspired.page.html',
  styleUrls: ['./be-inspired.page.scss'],
})
export class BeInspiredPage implements OnInit {

  creations = [];
  usernames = [];
  userId: string;
  pageTitle: string;
  pageSubtitle: string;
  likesRow: boolean;

  // Pagination variables
  lastKey: any;
  lastValue: any;
  pageSize: number;
  modelsLoading: any;
  noOlderData: boolean;
  severalCreations: boolean;

  // Database references
  dbRef: any;
  usersRef: any;
  likesRef: any;
  wishRef: any;
  boughtRef: any;
  boughtCntRef: any;
  soldRef: any;
  soldCntRef: any;
  soldToRef: any;
  chatRef: any;
  providerChatRef: any;
  clientChatRef: any;
  creationReportRef: any;
  favouriteDesignersRef: any;
  designerLikersRef: any;

  constructor(
    private params: NavParamsService,
    public alertController: AlertController,
    private socialSharing: SocialSharing,
    public navCtrl: NavController
  ) {
    this.createDbReferences();
    this.fetchCreations();
  }

  ngOnInit() {
    // Initializing the page variables
    this.pageTitle = 'My Wall';
    this.pageSubtitle = 'Latest designs';
    this.severalCreations = false;
    this.noOlderData = false;
    this.lastKey = null;
    this.lastValue = null;
    this.pageSize = 10;
    this.likesRow = true;
    this.userId = this.params.getLoggedUserId();
  }

  customizeOriginalModel(creation: any) {
    // Prepare the model object
    let model = {
      "producerId": creation.producerId,
      "modelName": creation.modelName,
      "category": creation.modelCategory,
      "modelId": creation.modelId
    }
    this.params.setReturnToModelsPage(false);
    this.params.setModel(model);
    // Pass the model object to the ModelCanvas page
    this.navCtrl.navigateForward('/modelCanvas');
  }

  ngOnDestroy() {
    // Terminate database references
    this.likesRef.off();
    this.wishRef.off();
    this.boughtRef.off();
    this.dbRef.off();
  }

  createDbReferences() {
    // Initialize database references
    this.dbRef = firebase.database().ref('/wall').orderByChild("date");
    this.usersRef = firebase.database().ref('/users');
    this.likesRef = firebase.database().ref('/creationslikes');
    this.wishRef = firebase.database().ref('/wishlist/'+this.userId);
    this.boughtRef = firebase.database().ref('bought/'+this.userId);
    this.boughtCntRef = firebase.database().ref('boughtCounter/'+this.userId);
    this.soldRef = firebase.database().ref('/sold');
    this.soldCntRef = firebase.database().ref('/soldCounter');
    this.soldToRef = firebase.database().ref('/soldTo/');
    this.chatRef = firebase.database().ref('/chats/');
    this.providerChatRef = firebase.database().ref('/providerChats/');
    this.clientChatRef = firebase.database().ref('/clientChat/');
    this.creationReportRef = firebase.database().ref('/creationReports');
    this.favouriteDesignersRef = firebase.database().ref('/myFaouriteDesigners');
    this.designerLikersRef = firebase.database().ref('/designerLikers');
  }

  reportCreation(creation: any, reportDetails: string) {
    // Save report details to the database
    this.creationReportRef.child(creation.creationId+'/').set({
      "reporter": this.userId,
      "date": -Date.now(),
      "description": reportDetails
    });
    // Send email with the report details
    this.socialSharing.shareViaEmail(reportDetails+" "+creation.creationId+" "+
    this.userId, 'Doro - Model Report', ['emailhere@domain.com']).then(() => {
      // Console.log("Email is sent.");
    }).catch(() => {
      // Catch error!
    });
  }

  addDesignerToFavourites(designerId: string) {
    if (this.userId != designerId) {
      this.favouriteDesignersRef
        .child(this.userId+'/'+designerId+'/').set({"like": true});
      this.designerLikersRef
        .child(designerId+'/'+this.userId+'/').set({"liker": true});
    }
  }

  removeDesignerFromFavourites(designerId: string) {
      this.favouriteDesignersRef
        .child(this.userId+'/'+designerId+'/').remove();
      this.designerLikersRef
        .child(designerId+'/'+this.userId+'/').remove();
  }

  likeCreation(creationId: string) {
    // Play like sound
    this.customSounds.play('like');
    this.likesRef.child(creationId).once('value', creationLikes => {

      // Update the creation like counter
      var counter = creationLikes.val().counter;
      counter = counter+1;
      console.log("Counter: ", counter);
      this.likesRef.child(creationId).update({'counter': counter});

      // Keep the id of the user who likes the creation
      this.likesRef
          .child(creationId+'/likers/'+this.userId).update({'like': true});

    });
  }

  dislikeCreation(creationId: string) {
    // Play dislike sound
    this.customSounds.play('dislike');
    this.likesRef.child(creationId).once('value', creationLikes => {

      // Update the creation like counter
      var counter = creationLikes.val().counter;
      counter = counter-1;
      console.log("Counter: ", counter);
      this.likesRef.child(creationId).update({'counter': counter});

      // Remove the id of the user who dislikes the creation
      this.likesRef.child(creationId+'/likers/'+this.userId).remove();
    });
  }

  fetchCreations() {
    var firstPage = true;
    let ref = this.dbRef;

    if (this.lastValue) {
      firstPage = false;
      // Set reference to the current creation
      ref = ref.startAt(this.lastValue, this.lastKey)
                                        .limitToFirst(this.pageSize+1);
    } else {
      firstPage = true;
      // Set reference to the first creation
      ref = ref.limitToFirst(this.pageSize);
    }

    ref.on('value', creations => {
      const keys = [];
      const data = [];
      var skipMarginalCreation = true;

      // Iterate through fetched creations
      creations.forEach(creation => {
        var cr = this.createArrayTemplate(creation);
        data.push(cr);
        keys.push(creation.key);

        if (!skipMarginalCreation)
            this.creations.push(cr);
        else {
          if (firstPage)
              this.creations.push(cr);
          else
            skipMarginalCreation = false;
        }

        if(this.creations.length > this.pageSize)
          this.severalCreations = true;
        var obj = this.creations.find(obj => obj.creationId == creation.key);

        // Fetch additional creation details
        this.isThisDesignerLikedByCurrentUser(obj);
        this.fetchUsername(creation, obj);
        this.fetchProviderName(creation, obj);
        this.fetchLikesCounter(creation, obj);
        this.isThisCreationLiked(creation, obj);
        this.isThisCreationInWishlist(creation, obj);
        this.isThisCreationBought(creation, obj);
      });

      // Skip the first value, which is actually the cursor
      if( this.lastValue !== null ) {
        keys.shift();
        data.shift();
      }

      // Store the last loaded record
      if( data.length ) {
        const last = data.length - 1;
        this.lastKey = keys[last];
        this.lastValue = -(data[last].creationDate);
      }

      // All data have been loaded
      if (data.length == 0) {
        this.noOlderData = true;
      }

     });
  }

  isThisDesignerLikedByCurrentUser(obj: any) {
    // Check if current user likes the designer of the creation
    this.favouriteDesignersRef.child(this.userId+'/'+obj.designerId+'/')
        .on('value', currentUser => {

      if (currentUser.val())
        obj.designerLikedByCurrentUser = currentUser.val().like;
      else
        obj.designerLikedByCurrentUser = false;
    });
  }

  fetchUsername(creation: any, obj: any) {
    // Fetch designer's name
    this.usersRef.child(creation.val().designerId).once('value', user => {
      obj.designerName = user.val().username;
    });
  }

  fetchProviderName(creation: any, obj: any) {
    // Fetch provider's name
    this.usersRef.child(creation.val().providerId).once('value', user => {
      obj.providerName = user.val().username;
    });
  }

  fetchLikesCounter(creation: any, obj: any) {
    // Fetch likes counter of the current creation
    this.likesRef.child(creation.key).on('value', count => {
      if (count.val())
        obj.likes = count.val().counter;
    });
  }

  isThisCreationLiked(creation: any, obj: any) {
    // Check if the current user likes the given creation
    this.likesRef.child(creation.key+'/likers/'+this.userId)
        .on('value', currentUser => {

      if (currentUser.val())
        obj.likedByCurrentUser = currentUser.val().like;
      else
        obj.likedByCurrentUser = false;
    });
  }

  isThisCreationInWishlist(creation: any, obj: any) {
    this.wishRef.child(creation.key).on('value', wish => {
      if (wish.val() != null) {
        obj.addedToUsersWishlist = wish.val().addedToUsersWishlist;
      }
      else {
        obj.addedToUsersWishlist = false;
      }
    });
  }

  isThisCreationBought(creation: any, obj: any) {
    this.boughtRef.child(creation.key).on('value', bought => {
      if (bought.val() != null)
        obj.boughtByCurrentUser = bought.val().bought;
      else {
        obj.boughtByCurrentUser = false;
      }
    });
  }


}
