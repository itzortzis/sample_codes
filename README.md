**This repository contains some sample codes I have written through the years**

---

## Files description

1. selected-chat.ts     Jan 10, 2020  Typescript
2. be_inspired.ts       May 10, 2019  Typescript
3. stripe_firebase.js   Sep 15, 2018  Nodejs
4. raspberry_gpio.cpp   Dec 10, 2015  C++
5. Indexing.java        Apr 10, 2015  Java
6. task_server.c        Apr 20, 2014  C
7. encoding.c           Feb 08, 2014  C
8. bloxorz.js           Jun 20, 2013  Javascript
9. btree.java           May 13, 2013  Java
10. srv_sayer.c         May 07, 2013  C

---
