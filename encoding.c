/*
 * File: encoding.c
 * Author: YianisTz
 * Description: Huffman encoding
 * #Huffman #encoding #datastructures #heap #tree
 *
 * Created on February 7, 2014
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "encoding.h"
#include "huffman_encoding.h"




node init_node(node n)
{
  // Allocate memory for new node and initialize its elements
  n = (node)malloc(sizeof(struct tree_node));
  n->parent = NULL;
  n->right_node = NULL;
  n->left_node = NULL;
  n->next = NULL;
  n->freq = NO_FREQ;
  n->value = 0;
  n->bin = (int *)malloc(K_BITS * sizeof(int));
  int i;
  for (i=0; i < K_BITS; i++) {
    n->bin[i] = EMPTY_CELL;
  }
  return n;
}

int **init_map_table(int **map_t, int map_t_size)
{
  // Initialize map table
  map_t = malloc(map_t_size * sizeof(int *));
  int i, j;
  for (i=0; i<map_t_size; i++) {
    map_t[i] = malloc((K_BITS+1) * sizeof(int));
  }

  for (i=0; i<map_t_size; i++) {
    for (j=0; j<(K_BITS+1); j++) {
      map_t[i][j] = EMPTY_CELL;
    }
  }
}


void print_node(node n)
{
  printf("Node:\n");
  printf("------\n\n");
  printf("\tValue: %d\n", n->value);
  printf("\tFrequency: %d\n", n->freq);
  printf("\tPointer: %d\n", n->pointer);
  if ((n->left_node == NULL) && (n->right_node == NULL)) {
    printf("\tNode is LEAF\n");
  }
  printf("\tNode map table: \n");
  int i = 0;
  while (i < n->pointer) {
    printf("\t\t-> %d\n", n->bin[i]);
    i++;
  }
  printf("\n\n");
}

huff_heap init_heap(huff_heap h_heap)
{
  //printf("Initializing heap...\n");
  h_heap = (huff_heap)malloc(sizeof(struct heap));
  h_heap->head = NULL;
  h_heap->tail = NULL;
  h_heap->counter = EMPTY_HEAP;
  return h_heap;
}

void print_heap(huff_heap h_heap)
{
  printf("\nPrinting the heap:\n");
  printf("-------------------\n\n");
  printf("Size of the heap: %d\n", h_heap->counter);
  node current;
  current = init_node(current);
  current = h_heap->head;
  while (current != NULL){
    print_node(current);
    current = current->next;
  }
  printf("Print_heap-> Free current node.\n");
  free(current);
  printf("End of heap printing\n");
}

void free_heap(huff_heap h_heap)
{
  printf("\nFree heap:\n");
  printf("--------------------------------------\n");
  node current, tmp;
  current = init_node(current);
  tmp = init_node(tmp);
  current = h_heap->head;
  while (current != NULL) {
    tmp = current;
    current = current->next;
    printf("Free node - Heap size: %d\n", h_heap->counter);
    h_heap->counter--;
    free(tmp);
  }
  printf("Free node - Heap size: %d\n", h_heap->counter);
  free(current);
  printf("Free heap structure\n");
  free(h_heap);
  printf("--------------------------------------\n");
}

void insert_to_heap(node new_node, huff_heap h_heap)
{
  // Insert new node to heap
  node current, tmp_node;
  current = init_node(current);
  tmp_node = init_node(tmp_node);

  if (h_heap->head == NULL) {
    // Heap is empty
    h_heap->head = new_node;
    h_heap->head->next = NULL;
  }
  else {
    // New node should be the new head of the heap
    if ((new_node->freq < h_heap->head->freq) || (new_node->freq ==
                              h_heap->head->freq)) {
      new_node->next = h_heap->head;
      h_heap->head = new_node;
    }
    else {
      current = h_heap->head;
      // Find the right place for the new node
      while ((current != NULL) && ((new_node->freq) > (current->freq) )) {
        tmp_node = current;
        current = current->next;
        tmp_node->next = current;
      }
      //insert the new node;
      tmp_node->next = new_node;
      new_node->next = current;
    }
  }
}

node is_in_heap(node some_node, huff_heap h_heap)
{
  // Check if a given node is already in the heap
  node current;
  node tmp;
  tmp = init_node(tmp);
  current = init_node(current);
  if (h_heap->head == NULL) {
    // Heap is empty => return NULL
    return NULL;
  }
  else {
    if (h_heap->head->value == some_node->value) {
      // Given node is on top of the heap
      some_node->freq = some_node->freq + h_heap->head->freq;
      h_heap->head = h_heap->head->next;
      h_heap->counter--;
      return some_node;
    }
    current = h_heap->head;
    tmp = h_heap->head;
    // Loop to find the given node
    while ((current != NULL) && (current->value != some_node->value)) {
      tmp = current;
      current = current->next;
    }
    if (current == NULL) {
      // The node was not found in the heap
      return NULL;
    }
    else {
      // The node was found in the heap
      tmp->next = current->next;
      current->next = NULL;
      h_heap->counter--;
      some_node->freq = some_node->freq + current->freq;
      free(current);
      return some_node;
    }
  }
}

void insert_value_to_heap(int value, huff_heap h_heap)
{
  // Insert a new value
  node tmp_node;
  node tmp;
  // Create a new node and assign the given value to it
  tmp = init_node(tmp);
  tmp_node = init_node(tmp_node);
  tmp_node->value = value;
  tmp_node->freq = DFLT_FREQ;

  tmp = is_in_heap(tmp_node, h_heap);

  if (tmp == NULL) {
    // Insert new node
    insert_to_heap(tmp_node, h_heap);
    h_heap->counter++;
  }
  else {
    // Update found node
    insert_to_heap(tmp, h_heap);
    h_heap->counter++;
  }

}

node delete_from_heap(huff_heap h_heap)
{
  node tmp;
  tmp = init_node(tmp);
  tmp = h_heap->head;
  h_heap->head = h_heap->head->next;
  h_heap->counter--;
  return tmp;
}

huff_tree init_huff_tree(huff_tree h_tree) {
  // Initialize huffman tree
  h_tree = (huff_tree)malloc(sizeof(struct tree));
  h_tree->root = NULL;
  h_tree->depth = EMPTY_TREE;
  return h_tree;
}

huff_tree build_huff_tree(huff_tree h_tree, huff_heap h_heap) {
  if (h_tree->root == NULL) {
    // Huffman tree is empty
    h_tree->root = init_node(h_tree->root);
    node left, right;
    if (h_heap->counter < 2) {
      return h_tree;
    }
    // Fetch nodes from heap
    left = delete_from_heap(h_heap);
    right = delete_from_heap(h_heap);
    h_tree->root->left_node = left;
    h_tree->root->right_node = right;
    h_tree->root->freq = left->freq + right->freq;
    h_tree->depth++;
    // Recursive function call
    build_huff_tree(h_tree, h_heap);
  }
  else {
    node left1;
    if (h_heap->counter < 1) {
      // Heap is empty
      return h_tree;
    }
    left1 = delete_from_heap(h_heap);

    node new_root;
    //printf("build_huff_tree new_root-> ");
    new_root = init_node(new_root);
    new_root->right_node = h_tree->root;
    new_root->left_node = left1;
    new_root->freq = h_tree->root->freq + left1->freq;
    h_tree->root = new_root;
    h_tree->depth++;

    // Recursive function call
    build_huff_tree(h_tree, h_heap);
  }
}

huff_tree build_wide_huff_tree(huff_tree h_tree, huff_heap h_heap)
{
  if (h_tree->root == NULL) {
    //printf("Build_huff_tree root-> ");
    h_tree->root = init_node(h_tree->root);
    node left, right;
    if (h_heap->counter < 2) {
      return h_tree;
    }
    // Fetch nodes from heap
    left = delete_from_heap(h_heap);
    right = delete_from_heap(h_heap);
    h_tree->root->left_node = left;
    h_tree->root->right_node = right;
    h_tree->root->freq = left->freq + right->freq;
    h_tree->depth++;
    // Recursive function call
    build_wide_huff_tree(h_tree, h_heap);
  }
  else {
	if (h_heap->counter < 1) {
      // Heap is empty
      return h_tree;
    }
    node tmp_1;
    tmp_1 = delete_from_heap(h_heap);

    if ((h_heap->head != NULL) && (h_heap->head->freq <= h_tree->root->freq)){
      if (h_heap->counter < 1) {
        //printf("Heap is empty\n");
        return h_tree;
      }
      node tmp_2;
      tmp_2 = delete_from_heap(h_heap);
      node sub_root;
      sub_root = init_node(sub_root);
      sub_root->freq = tmp_1->freq + tmp_2->freq;
      sub_root->left_node = tmp_1;
      sub_root->right_node = tmp_2;

      node new_root;
      new_root = init_node(new_root);
      new_root->left_node = h_tree->root;
      new_root->right_node = sub_root;

      h_tree->root = new_root;
      // Recursive function call
      build_wide_huff_tree(h_tree, h_heap);
    }
    else {

      node new_root;
      new_root = init_node(new_root);
      new_root->left_node = h_tree->root;
      new_root->right_node = tmp_1;
      new_root->freq = new_root->left_node->freq + new_root->right_node->freq;

      h_tree->depth++;
      h_tree->root = new_root;
      //printf("\n-----------------> Depth: %d\n\n", h_tree->depth);
      if (h_heap->counter < 1) {
        //printf("Heap is empty\n");
        return h_tree;
      }
      build_wide_huff_tree(h_tree, h_heap);
    }
  }
}


void print_huff_tree_preorder(node root)
{
  printf("-----------------------------\n\n");
  //print_node(root);
  if ((root->left_node == NULL) && (root->right_node == NULL)) {
    print_node(root);
    return;
  }
  else {
    if ((root->left_node != NULL) && (root->right_node == NULL)) {
      print_huff_tree_preorder(root->left_node);
      print_node(root);
      return;
    }
    else if ((root->left_node == NULL) && (root->right_node != NULL)) {
      print_node(root);
      print_huff_tree_preorder(root->right_node);
      return;
    }
    else {
      print_huff_tree_preorder(root->left_node);
      print_node(root);
      print_huff_tree_preorder(root->right_node);
      return;
    }
  }
  printf("End of huffman tree printing\n");
  printf("-----------------------------\n\n");
}


void update_huff_tree(node root, huff_tree h_tree, int *bin, int p, int bit)
{
  if (root == h_tree->root) {
    //printf("CASE 1\n");

    update_huff_tree(root->left_node, h_tree, root->bin,
                          root->pointer, BIT_0);

    update_huff_tree(root->right_node, h_tree, root->bin,
                          root->pointer, BIT_1);
  }
  else {
    if ((root->left_node == NULL) && (root->right_node == NULL)) {
      //if (root->flag == UNSET) {
        int i = 0;
        while ((i < K_BITS) && (bin[i] != EMPTY_CELL)) {
          root->bin[i] = bin[i];
          i++;
        }
        root->bin[p] = bit;
        root->pointer = p+1;
        //print_node(root);
        //printf("CASE 2\n");
      //}

    }
    else {
      int i = 0;
      while ((i < K_BITS) && (bin[i] != EMPTY_CELL)) {
        root->bin[i] = bin[i];
        i++;
      }
      root->bin[p] = bit;
      root->pointer = p+1;
      //print_node(root);
      //printf("CASE 5\n");

      if (root->left_node != NULL) {
        update_huff_tree(root->left_node, h_tree, root->bin,
                            root->pointer, BIT_0);
      }
      if (root->right_node != NULL) {
        update_huff_tree(root->right_node, h_tree, root->bin,
                             root->pointer, BIT_1);
      }
    }
  }
}


void search_huff_tree(node current, int value)
{
  if ((current->left_node == NULL) && (current->right_node == NULL)) {
    // Leaf node
    if (current->value == value) {
      // The value was found
      found = 1;
      found_node = current;
    }
  }
  else {
    if (current->left_node != NULL) {
      // Left child
      search_huff_tree(current->left_node, value);
    }
    if (found == 1){
      return;
    }
    if (current->right_node !=NULL) {
      // Right child
      search_huff_tree(current->right_node, value);
    }
  }
}


void free_huff_tree(node root)
{
  if ((root->left_node == NULL) && (root->right_node == NULL)) {
    free(root);
    //printf("Node is free.\n");
    return;
  }
  else {
    if ((root->left_node != NULL) && (root->right_node == NULL)) {
      free_huff_tree(root->left_node);
      free(root);
    }
    else if ((root->left_node == NULL) && (root->right_node != NULL)) {
      node tmp;
      tmp = init_node(tmp);
      tmp = root;
      free(root);
      free_huff_tree(root->right_node);
    }
    /*else {
      free_huff_tree(root->left_node);
      node tmp;
      tmp = init_node(tmp);
      tmp = root;
      free(root);
      free_huff_tree(root->right_node);
    }*/
  }
  //printf("\nFree_huff_tree has finished successfully\n");
}

void build_map_table(int elems, int mode, huff_heap h_heap)
{
  if (mode == INS_SMBLS_MODE) {
    node current;
    current = init_node(current);
    current = h_heap->head;
    int i = 0;
    while (current != NULL){
      map_table[i][0] = current->value;
      current = current->next;
    }
  }
}
