/*
 * File:   encoding.c
 * Project: Huffman encoding
 * Author: YianisTz
 * Description: This file contains part of the server program used for an IRC
 * based communication platform.
 * #Server #sochets #threads #networking #condvariables
 *
 * Created on May 7, 2013
 */


#include <stdio.h>
#include "mylib.h"

/*
stall_sender:
--------------

 this function is called to lock the mutex for the client "sender"
*/
void *stall_sender(void *mes)
{
	clnt_manager = (client_manager *) mes;
	LOCK(clnt_manager->mux); // Lock the mutex
	while (clnt_manager->receiver_turn){
		// Waiting for not_receiver condition variable
		WAIT(clnt_manager->not_receiver, clnt_manager->mux);
	}
	init_sender_con(); // Establish connection and exchange messages
	UNLOCK(clnt_manager->mux); // Release the mutex
	clnt_manager->sender_turn = OFF;
	clnt_manager->receiver_turn = ON;
	SIGNAL(clnt_manager->not_sender); // Send CV to release the receiver threads
}

/*
init_sender_con:
-----------------

	This function is used for the creation and initialization of the communication
	endpoint for the clients "sender".
*/
void init_sender_con()
{
	int binded_sock, listen_sock, accepted_con, i;
	struct sockaddr_in client;
	server_sock = create_socket(); // Create new socket for the sender connections
	// Bind the created socket to the specified port
	binded_sock = bind_socket(server_sock, TWITSAY_PORT);
	// Listening for incoming connections
	listen_sock = listen_socket(server_sock, CONNECTIONS_NUM);
	sender_threads(server_sock);
}



/*
init_sender_elems:
------------

 this function is used to initialize the structure of sender
*/
void *init_sender_elems(void)
{
	sender_manager *new_sender;
	new_sender = (sender_manager *)malloc(sizeof(sender_manager));
	new_sender->thread_id = 0;
	new_sender->sockfd = 0;

	MUT_INIT(new_sender->mut, NULL);

	return new_sender;
}


/*
sender_threads:
---------------

 this function creates a new thread for each of the incoming sender
 connections
*/
void sender_threads(int sock_fd)
{

	int accepted_con, i=0;
	pthread_t sender_th;
  REC_FLAG = HIGH;
	while(REC_FLAG){
    say_connections++;
		// Creating new thread that executes the function 'sender_proc'
		pthread_create(&sender_th, NULL, sender_proc, NULL);
		pthread_join(sender_th, NULL);
		i++;
	}
	return;
}


/*
sender_proc:
------------

 this function is being executed by any new sender thread
*/
void *sender_proc(void *sock_fd)
{
  struct sockaddr_in client;
	char buffer[MSG_LEN];
  int accepted_con;

  accepted_con = accept_con(server_sock, client);
	printf(">A new 'saye' connected!\n");
  recv(accepted_con, buffer, sizeof(buffer), 0);
  printf("< %s\n", buffer);
  enqueue(buffer);
  char *str = "Hi sender, I'm server. We got a connection :)";
  send_msg(accepted_con, str);
	thread_counter--;
	pthread_exit(NULL);
}
