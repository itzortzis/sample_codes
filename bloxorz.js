
/*
 * File: bloxorz.c
 * Author: YianisTz
 * Description: Mechanics for a clone of Bloxorz game.
 * #Javascript #WebGL #mechanics #Bloxorz #game
 *
 * Created on June 20, 2013
 */

  function initGL(canvas) {
    try {
		//get a webgl context
      gl = canvas.getContext("experimental-webgl");
			//assign a viewport width and height based on the HTML canvas element properties
			//(check last lines of code)
      gl.viewportWidth = canvas.width;
      gl.viewportHeight = canvas.height;
			//any error is handled here
			//all errors are visible in the console (F12 in Google chrome)
    } catch (e) {
    }
    if (!gl) {
      alert("Could not initialise WebGL, sorry :-(");
    }
  }


	//Find and compile shaders (vertex + fragment shader)
  function getShader(gl, id) {
	//gets the shader scripts (vertex + fragment)
    var shaderScript = document.getElementById(id);
    if (!shaderScript) {
      return null;
    }

    var str = "";
    var k = shaderScript.firstChild;
    while (k) {
      if (k.nodeType == 3) {
        str += k.textContent;
      }
      k = k.nextSibling;
    }

    var shader;
		//create shaders
    if (shaderScript.type == "x-shader/x-fragment") {
      shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderScript.type == "x-shader/x-vertex") {
      shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
      return null;
    }

		//ask WebGL to compile shaders
		//we check for errors here too
		//all errors are visible in the console (F12 in Google chrome)
    gl.shaderSource(shader, str);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
      alert(gl.getShaderInfoLog(shader));
      return null;
    }

    return shader;
  }



  var shaderProgram;

	//Creates a program from a vertex + fragment shader pair
  function initShaders() {
    var fragmentShader = getShader(gl, "shader-fs");
    var vertexShader = getShader(gl, "shader-vs");

    shaderProgram = gl.createProgram();
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
		//link the compiled binaries
    gl.linkProgram(shaderProgram);

		//check for errors, again
    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
      alert("Could not initialise shaders");
    }

		//activate current program
		//this sandbox has only on shader pair
		//we can have as many as we wish in more complex applications
    gl.useProgram(shaderProgram);

		//Update attributes for the vertex shader
		//attributes are accessible only from the vertex shader
		//if we want accessible data from a fragment shader we can use uniform variables,
		//or varyings that will be forwarded from the vertex shader to the fragment shader

		//Vertex position data
    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

		//Vertex color data
    //shaderProgram.vertexColorAttribute = gl.getAttribLocation(shaderProgram, "aVertexColor");
    //gl.enableVertexAttribArray(shaderProgram.vertexColorAttribute);

    shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);


		//Update uniform variables
		//this variables can be accessed from both the vertex and fragment shader
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
  }


/*

 | HandleLoadedTexture function
 +--------------------------------------------------------------------------------------------------------------
*/
  function handleLoadedTexture(texture) {
    // Set the properties of textures
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.bindTexture(gl.TEXTURE_2D, null);
  }

/*

 | InitTexture function
 +--------------------------------------------------------------------------------------------------------------
*/
  var neheTexture;
  function initTexture() {
    // Initialization of textures
    neheTexture = gl.createTexture();
    neheTexture.image = new Image();
    neheTexture.image.crossOrigin = "anonymous";
    neheTexture.image.onload = function() {
      handleLoadedTexture(neheTexture)
    }

    neheTexture.image.src = "images/mytex.gif";
  }

	//ModelView and Projection matrices
	//mat4 comes from the external library
  var mvMatrix = mat4.create();
  var mvMatrixStack = [];
  var pMatrix = mat4.create();

	//The matrix stack operation are implemented below to handle local transformations

	//Push Matrix Operation
  function mvPushMatrix() {
    var copy = mat4.create();
    mat4.set(mvMatrix, copy);
    mvMatrixStack.push(copy);
  }

	//Pop Matrix Operation
  function mvPopMatrix() {
    if (mvMatrixStack.length == 0) {
      throw "Invalid popMatrix!";
    }
    mvMatrix = mvMatrixStack.pop();
  }


	//Sets + Updates matrix uniforms
  function setMatrixUniforms() {
    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
  }


/*

 | Rotation function helper
 +--------------------------------------------------------------------------------------------------------------
*/
  function degToRad(degrees) {
    return degrees * Math.PI / 180;
  }


	//Vertex, Index and Color Data
  var cubeVertexPositionBuffer; // contains coordinates
  var cubeVertexColorBuffer; //contains color per vertex
  var cubeVertexIndexBuffer; //contains indices for chains of vertices to draw triangles/other geometry


/*

 | initBuffers function
 +--------------------------------------------------------------------------------------------------------------
*/
	//Initialize VBOs, IBOs and color
  function initBuffers() {
    //Vertex Buffer Object
    cubeVertexPositionBuffer = gl.createBuffer();
		//Bind buffer to ARRAY_BUFFER
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
    vertices = [
      -1.0, -1.0,  3.0,
       1.0, -1.0,  3.0,
       1.0,  1.0,  3.0,
      -1.0,  1.0,  3.0,

      // Back face
      -1.0, -1.0, -1.0,
      -1.0,  1.0, -1.0,
       1.0,  1.0, -1.0,
       1.0, -1.0, -1.0,

      // Top face
      -1.0,  1.0, -1.0,
      -1.0,  1.0,  3.0,
       1.0,  1.0,  3.0,
       1.0,  1.0, -1.0,

      // Bottom face
      -1.0, -1.0, -1.0,
       1.0, -1.0, -1.0,
       1.0, -1.0,  3.0,
      -1.0, -1.0,  3.0,

      // Right face
       1.0, -1.0, -1.0,
       1.0,  1.0, -1.0,
       1.0,  1.0,  3.0,
       1.0, -1.0,  3.0,

      // Left face
      -1.0, -1.0, -1.0,
      -1.0, -1.0,  3.0,
      -1.0,  1.0,  3.0,
      -1.0,  1.0, -1.0
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);
		//every item has 3 coordinates (x,y,z)
    cubeVertexPositionBuffer.itemSize = 3;
		//we have 24 vertices
    cubeVertexPositionBuffer.numItems = 24;

    //------------------- Textures --------------------
	cubeVertexTextureCoordBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
    var textureCoords = [
      // Front face
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,

      // Back face
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      0.0, 0.0,

      // Top face
      0.0, 1.0,
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,

      // Bottom face
      1.0, 1.0,
      0.0, 1.0,
      0.0, 0.0,
      1.0, 0.0,

      // Right face
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
      0.0, 0.0,

      // Left face
      0.0, 0.0,
      1.0, 0.0,
      1.0, 1.0,
      0.0, 1.0,
    ];
    gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureCoords), gl.STATIC_DRAW);
    cubeVertexTextureCoordBuffer.itemSize = 2;
    cubeVertexTextureCoordBuffer.numItems = 24;

    cubeVertexIndexBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);
    var cubeVertexIndices = [
      0, 1, 2,    0, 2, 3,  // Front face
      4, 5, 6,    4, 6, 7,  // Back face
      8, 9, 10,   8, 10, 11,  // Top face
      12, 13, 14,   12, 14, 15, // Bottom face
      16, 17, 18,   16, 18, 19, // Right face
      20, 21, 22,   20, 22, 23  // Left face
    ];
    gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(cubeVertexIndices), gl.STATIC_DRAW);
    cubeVertexIndexBuffer.itemSize = 1;
    cubeVertexIndexBuffer.numItems = 36;
  }


	//Helper Variables
  var rCube = 0;
  var xTrans = 0.0;
  var yTrans = 0.0;

  //array for keeping pressed keys
  var currentlyPressedKeys = {};


/*

 | Keyboard handler function
 +--------------------------------------------------------------------------------------------------------------
*/
	//do not touch :)
  function handleKeyDown(event) {
    currentlyPressedKeys[event.keyCode] = true;
	//console.log("key down");
    if (String.fromCharCode(event.keyCode) == "F") {
      filter += 1;
      if (filter == 3) {
        filter = 0;
      }
    }
  }



/*

 | Keyboard handler function
 +--------------------------------------------------------------------------------------------------------------
*/

	//do not touch :)
  function handleKeyUp(event) {
    currentlyPressedKeys[event.keyCode] = false;
  }

  var rotate_x = 0; // Enables rotation across x axis
  var rotate_y = 0; // Enables rotation across y axis
  var rotate_z = 0; // Enables rotation across z axis

  var deg_x = 0; // Degrees to rotate brick across x axis
  var deg_y = 0; // Degrees to rotate brick across y axis

  var hor_state = 0;  // Horizontal state of the cube
  var ver_state = 0;  // Vertical state of the cube


  var rightTrans = 1; // Right direction
  var leftTrans = 0;  // Left direction
  var downTrans = 0;  // Down direction
  var upTrans = 1;  // Up direction

  var level = 0; // Defines the current level
  var levelArray; // Array of each level

  var end_x = 0; // X coordinate of end point
  var end_y = 0; // Y coordinate of end point

  var start_x = 0.0; // X coordinate of start point
  var start_y = 0.0; // Y coordinate of start point

  //Level array
  function initLevel(){
    if (level == 0){
      // First level with its properties
      start_x = 1.0;
      start_y = -1;
      deg_x = 0;
      deg_y = 0;
      xTrans = start_x;
      yTrans = start_y;

      levelArray = [
        ["p", "p", "p", "x", "x", "x", "x", "x", "x", "x"],
        ["p", "s", "p", "p", "p", "p", "x", "x", "x", "x"],
        ["p", "p", "p", "p", "p", "p", "p", "p", "p", "x"],
        ["x", "p", "p", "p", "p", "p", "p", "p", "p", "p"],
        ["x", "x", "x", "x", "x", "p", "p", "e", "p", "p"],
        ["x", "x", "x", "x", "x", "x", "p", "p", "p", "x"]
      ];

      end_x = 7;
      end_y = -4;
    }
    else if (level == 1){
      // Second level with its properties
      start_x = 0.0;
      start_y = -3;
      deg_x = 0;
      deg_y = 0;
      xTrans = start_x;
      yTrans = start_y;
      levelArray = [
        ["p", "p", "p", "p", "p", "x", "x", "x", "x", "x"],
        ["p", "x", "x", "p", "p", "p", "p", "p", "p", "p"],
        ["p", "x", "p", "p", "p", "x", "x", "p", "p", "p"],
        ["p", "x", "p", "p", "p", "x", "x", "p", "e", "p"],
        ["x", "x", "p", "p", "p", "x", "x", "p", "p", "p"],
        ["x", "x", "x", "x", "x", "x", "x", "x", "x", "x"]
      ];
      end_x = 8;
      end_y = -3;
    }
  }
/*

 | handleKeys function
 +--------------------------------------------------------------------------------------------------------------
*/
	//Key pressed callback
	//37-40 are the codes for the arrow keys
	//xTrans + yTrans are used in the ModelView matrix for local transformation of the cube
  function handleKeys() {
    if (currentlyPressedKeys[37]) {
      // Left cursor key

      if ((hor_state == 0) && (ver_state == 0)){
        // brick is standing
        xTrans -= 1;
        console.log("xTrans:"+xTrans);
        rotate_y = 1;
        rotate_x = 0;
        deg_y = -90;
        hor_state = 1;
        ver_state = 0;
        xTransHandler(leftTrans);
      }
      else if ((hor_state == 0) && (ver_state == 1)){
        // Brick lies across y axis
        xTrans -= 1;
        console.log("xTrans:"+xTrans);
        hor_state = 0;
        ver_state = 1;
        xTransHandler(leftTrans);
      }
      else if ((hor_state == 1) && (ver_state == 0)){
        // Brick lies across x axis
        if (deg_y == -90){
          xTrans -= 2;
        }
        else{
          xTrans -= 1;
        }
        console.log("xTrans:"+xTrans);
        rotate_y = 1;
        rotate_x = 0;
        deg_y = 0;
        hor_state = 0;
        ver_state = 0;
        xTransHandler(leftTrans);
      }
      currentlyPressedKeys[37] = false;

    }
    if (currentlyPressedKeys[39]) {
      // Right cursor key
      if ((hor_state == 0) && (ver_state == 0)){
        // brick is standing
        xTrans += 1;
        console.log("xTrans:"+xTrans);
        rotate_y = 1;
        rotate_x = 0;
        deg_y = 90;
        hor_state = 1;
        ver_state = 0;
        xTransHandler(rightTrans);
      }
      else if ((hor_state == 0) && (ver_state == 1)){
        // Brick lies across y axis
        xTrans += 1;
        console.log("xTrans:"+xTrans);
        hor_state = 0;
        ver_state = 1;
        xTransHandler(rightTrans);
      }
      else if ((hor_state == 1) && (ver_state == 0)){
        // Brick lies across x axis
        if (deg_y == 90){
          xTrans +=2;
        }
        else{
          xTrans += 1;
        }
        console.log("xTrans:"+xTrans);
        rotate_y = 1;
        rotate_x = 0;
        deg_y = 0;
        hor_state = 0;
        ver_state = 0;
        xTransHandler(rightTrans);
      }
      currentlyPressedKeys[39] = false;

    }
    if (currentlyPressedKeys[38]) {
      // Up cursor key
      if ((hor_state == 0) && (ver_state == 0)){
        // brick is standing
        yTrans += 2;
        console.log("yTrans:"+yTrans);
        rotate_y = 0;
        rotate_x = 1;
        deg_x = 90;
        console.log("Deg_x:"+deg_x);
        ver_state = 1;
        hor_state = 0;
        yTransHandler(upTrans);
      }
      else if ((hor_state == 0) && (ver_state == 1)){
        // Brick lies across y axis
        if (deg_x == 90){
          yTrans +=1;
        }
        else{
          yTrans += 2;
        }
        console.log("yTrans:"+yTrans);
        rotate_y = 0;
        rotate_x = 1;
        deg_x = 0;
        ver_state = 0;
        hor_state = 0;
        yTransHandler(upTrans);
      }
      else if ((hor_state == 1) && (ver_state == 0)){
        // Brick lies across x axis
        yTrans += 1;
        console.log("yTrans:"+yTrans);
        ver_state = 0;
        hor_state = 1;
        yTransHandler(upTrans);
      }
      currentlyPressedKeys[38] = false;
      //animate();
      //rCube += 90;
    }
    if (currentlyPressedKeys[40]) {
      // Down cursor key
      if ((hor_state == 0) && (ver_state == 0)){
        // brick is standing
        yTrans -= 2;
        console.log("yTrans:"+yTrans);
        rotate_y = 0;
        rotate_x = 1;
        deg_x = -90;
        console.log("Deg_x:"+deg_x);
        ver_state = 1;
        hor_state = 0;
        yTransHandler(downTrans);
      }
      else if ((hor_state == 0) && (ver_state == 1)){
        // Brick lies across y axis
        if (deg_x == -90){
          yTrans -=1;
        }
        else{
          yTrans -= 2;
        }
        console.log("yTrans:"+yTrans);
        rotate_y = 0;
        rotate_x = 1;
        deg_x = 0;
        ver_state = 0;
        hor_state = 0;
        yTransHandler(downTrans);
      }
      else if ((hor_state == 1) && (ver_state == 0)){
        // Brick lies across x axis
        yTrans -= 1;
        console.log("yTrans:"+yTrans);
        ver_state = 0;
        hor_state = 1;
        yTransHandler(downTrans);
      }
      currentlyPressedKeys[40] = false;
      //animate();
      //rCube -= 90;
    }
  }


/*

 | initPosition function
 +--------------------------------------------------------------------------------------------------------------
*/
  function initPosition(){
    xTrans = start_x;
    yTrans = start_y;
    rotate_x = 0;
    rotate_y = 0;
    hor_state = 0;
    ver_state = 0;
  }

/*

 | xTransHandler function
 +--------------------------------------------------------------------------------------------------------------
*/
  function xTransHandler(direction){
    if ((xTrans == end_x) && (yTrans == end_y) && (deg_x == 0) && (deg_y == 0) && (level == 0)){
      level = 1; // change level
      initLevel();
      //window.location = "congratulations.html";
    }
    else if ((xTrans == end_x) && (yTrans == end_y) && (deg_x == 0) && (deg_y == 0) && (level == 1)){
      window.location = "congratulations.html";
    }
    if (direction == leftTrans){

      if (xTrans<0){
        initPosition();
      }

      if ((xTrans == 0) && (deg_y == -90)){
        initPosition();
      }

      if ((deg_y == -90) && (levelArray[-yTrans][xTrans-1] == "x")){
        initPosition();
      }
      if ((deg_x == 90) && (levelArray[-yTrans+1][xTrans] == "x") && (yTrans != -5)){
         initPosition();
      }
    }
    if (direction == rightTrans){

      if (xTrans>9){
        initPosition();
      }

      if ((xTrans == 9) && (deg_y == 90)){
        initPosition();
      }

      if ((deg_y == 90) && (levelArray[-yTrans][xTrans+1] == "x")){
        initPosition();
      }
    }
    if ((levelArray[-yTrans][xTrans] == "x")){
      initPosition();
    }
    if ((yTrans != 0) && (deg_x == -90) && (levelArray[-yTrans-1][xTrans] == "x")){
      initPosition();
    }
  }


/*

 | yTransHandler function
 +--------------------------------------------------------------------------------------------------------------
*/
  function yTransHandler(direction){
    if ((xTrans == end_x) && (yTrans == end_y) && (deg_x == 0) && (deg_y == 0) && (level == 0)){
      level = 1;
      initLevel();
      //window.location = "congratulations.html";
    }
    else if ((xTrans == end_x) && (yTrans == end_y) && (deg_x == 0) && (deg_y == 0) && (level == 1)){
      window.location = "congratulations.html";
    }
    if (direction == upTrans){
      if ((yTrans>0)){
        initPosition();
      }
      if ((yTrans == 0) && (deg_x == -90)){
        initPosition();
      }
    }
    if (direction == downTrans){
      if ((yTrans<-5)){
        initPosition();
      }
      if ((yTrans == -5) && (deg_x == 90)){
        initPosition();
      }
    }
    if ((levelArray[-yTrans][xTrans] == "x")){
      initPosition();
    }
    if ((xTrans != 0) && (deg_y == 90) && (levelArray[-yTrans][xTrans+1] == "x")){
      initPosition();
    }
  }

/*

 | DrawLevel function
 +--------------------------------------------------------------------------------------------------------------
*/
  function drawLevel(){
    mvPushMatrix();
    var rowLength = 10;
    var colLength = 6;

	//mat4.translate(mvMatrix, [0, 0, 0]);
    mat4.translate(mvMatrix, [-4, 2, -10.0]);
    //mat4.rotate(mvMatrix, degToRad(-50), [1, 0, 0]);
	//mat4.rotate(mvMatrix, degToRad(-60), [1, 0, 0]);
	//mat4.rotate(mvMatrix, degToRad(-80), [0, 1, 0]);
	//mat4.rotate(mvMatrix, degToRad(-10), [0, 0, 1]);

	for (var i=0; i<6; i++){
      //mat4.translate(mvMatrix, [-1.0, 0.0, 0.0]);
      for(var j=0; j<10; j++){
        if ((levelArray[i][j] != "x") && (levelArray[i][j] != "e")){
		    mvPushMatrix();

			mat4.translate(mvMatrix, [j, -i, 0.0]);
	        mat4.scale(mvMatrix,[0.5, 0.5, 0.1],mvMatrix);

		    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
		    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

			//we bind the buffer for the cube colors
		    //gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexColorBuffer);
		    //gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, cubeVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);


             //we bind the buffer for the tile textures
             gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
             gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

             gl.activeTexture(gl.TEXTURE0);
             gl.bindTexture(gl.TEXTURE_2D, neheTexture);
             gl.uniform1i(shaderProgram.samplerUniform, 0);
			//we bind the buffer for the cube vertex indices
		    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);

			//we update the uniforms for the shaders
		    setMatrixUniforms();

			//we call the Draw Call of WebGL to draw the cube
			//Triangles mode
		    gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

			//we pop the matrix and now the active ModelView matrix is the global one
		    mvPopMatrix();
        }
      }
    }
  }

/*

 | DrawCube function
 +--------------------------------------------------------------------------------------------------------------
*/
  function drawCube(){

    //we use the matrix stack to employ a local transformation to the cube
	mvPushMatrix();

    //initCubePosition();

	//a translation associated with the keyboard
    mat4.translate(mvMatrix, [-4, 2, -6.4]);
	mat4.translate(mvMatrix, [xTrans, yTrans, -2.9]);
	mat4.scale(mvMatrix, [0.5, 0.5, 0.5]);

    //mat4.rotate(mvMatrix, degToRad(-60), [1, 0, 0]);
	//a rotation connected with animation parameters
	//mat4.rotate(mvMatrix, degToRad(0), [rotate_y, 0, 0]);
	mat4.rotate(mvMatrix, degToRad(deg_y), [0, rotate_y, 0]);
    mat4.rotate(mvMatrix, degToRad(deg_x), [rotate_x, 0, 0]);
	//mat4.rotate(mvMatrix, degToRad(0), [0, 0, rotate_y]);


	//we bind the buffer for the cube vertices
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexPositionBuffer);
    gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, cubeVertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

	//we bind the buffer for the cube colors
    //gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexColorBuffer);
    //gl.vertexAttribPointer(shaderProgram.vertexColorAttribute, cubeVertexColorBuffer.itemSize, gl.FLOAT, false, 0, 0);

    //we bind the buffer for the cube textures
    gl.bindBuffer(gl.ARRAY_BUFFER, cubeVertexTextureCoordBuffer);
    gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, cubeVertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

    gl.activeTexture(gl.TEXTURE0);
    gl.bindTexture(gl.TEXTURE_2D, neheTexture);
    gl.uniform1i(shaderProgram.samplerUniform, 0);

    //we bind the buffer for the cube vertex indices
    gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, cubeVertexIndexBuffer);

	//we update the uniforms for the shaders
    setMatrixUniforms();

	//we call the Draw Call of WebGL to draw the cube
	//Triangles mode
    gl.drawElements(gl.TRIANGLES, cubeVertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

	//we pop the matrix and now the active ModelView matrix is the global one
    mvPopMatrix();


  }


/*

 | DrawScene function
 +--------------------------------------------------------------------------------------------------------------
*/
	//For every frame this function draws the complete scene from the beginning
  function drawScene() {
	//the viewport gets the canvas values (that were assigned to the gl context variable)
    gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
	//the frame and depth buffers get cleaned (the depth buffer is used for sorting fragments)
	//without the depth buffer WebGL does not know which fragment is visible for a given pixel
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	//the projection matrix (pMatrix) is set
	//45 degrees Field-Of-View
	//aspect ratio gl.viewportWidth / gl.viewportHeight
	//near plane: 0.1 , far plane: 100
    mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 100.0, pMatrix);

	//the modelview Matrix is initialized with the Identity Matrix
    mat4.identity(mvMatrix);


	//the ModelView matrix gets a global transformation ("camera" retracts 8 units)
	//otherwise the "camera" will be inside the rotating cube
	//z-axis points out of the screen. we translate -8 which is the inverse transform
	//in essence we move the world -8 units to have the camera 8 units forward.
	//REMEMBER there is no actual camera in WebGL
	mat4.translate(mvMatrix, [-1, 8, -4.0]);
    mat4.rotate(mvMatrix, degToRad(-50), [1, 0, 0]);

    drawCube();
    drawLevel();

  }

	//animation parameter
  var lastTime = 0;


/*

 | Animate function
 +--------------------------------------------------------------------------------------------------------------
*/
	//Animate function
  function animate() {
    var timeNow = new Date().getTime();
    if (lastTime != 0) {
      var elapsed = timeNow+20 - lastTime;

			//adjust a constant rotation speed independently of platform/framerate
      rCube -= (300 * elapsed) / 1000.0;
    }
    lastTime = timeNow;
  }



/*

 | Tick function
 +--------------------------------------------------------------------------------------------------------------
*/
	//this is the requestAnimFrame callback
	//For every tick, request another frame
	//handle keyboard, draw the scene, animate (update animation variebles) and continue
  function tick() {
    requestAnimFrame(tick);
		handleKeys();
    drawScene();
    //animate();
  }

	//Entry point of the WebGL context
	function webGLStart() {
    var canvas = document.getElementById("TUCWebGL");

		//Functions for initialization
		//Check above
    initLevel();
    initGL(canvas);
    initShaders()

    initBuffers();
    initTexture();
    //initTile();
		//Background Color: Color assigned for all pixels with no corresponding fragments
    gl.clearColor(0.3, 0.3, 0.3, 1.0);

		//Enable z-buffer for depth sorting
    gl.enable(gl.DEPTH_TEST);

		//define the keyboard handlers
		document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

		//the first tick of our application
    tick();
  }
//end of Javascript

  function level_0(){
    level = 0;
    initLevel();
  }

  function level_1(){
    level = 1;
    initLevel();
  }
