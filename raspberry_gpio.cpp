/*
 * File: encoding.c
 * Author: YianisTz
 * Description: Main control program for prototype regenerative braking system
 * #Raspberry #control #supercapacitors #prototype #gpio
 *
 * Created on December 10, 2015
 */

#include <iostream>
#include <unistd.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include "gpio_class.h"
#include "aux_library.h"

using namespace std;

gpio_class* sw_fc;    // gpio object to control fuel cell switch
gpio_class* sw_sc_c;  // gpio object to control supercapacitors charging switch
gpio_class* sw_sc_d;  // gpio object to control supercapacitors
                                                           //discharging switch
gpio_class* t;  // gpio object for the  trottle switch
gpio_class* ch;  // gpio object for the boolean status ch - charged, not charged


int main (void)
{

	signal(SIGINT, quit_signal_handler);
	string t_value, ch_value, old_t, old_ch;
	print_logo();

	gpio_init_process();
	op_states = P;
	throttle_counter = 0;

	while (true) {

		if ((op_states == P) || (op_states == Q) || (op_states == R)) {
			automaton_init_op();
		}
		else if ((op_states == C) || (op_states == D) ||
						(op_states == E) || (op_states == F)) {
			automaton_charging_op();
		}
		else if ((op_states == H) || (op_states == I) ||
						(op_states == J) || (op_states == K)) {
			automaton_discharging_op();
		}
		else if ((op_states == L) || (op_states == M) ||
						(op_states == N) || (op_states == O)) {
			automaton_fc_op();
		}
		else if (op_states == A) {
			cout << "\nSTATE_A\n" << endl;
			usleep(SEC_TIME);
			t -> getval_gpio(t_value);

			if (t_value == T_PRESSED)
				op_states = G;
			//else
			//	op_states = C;
			if (throttle_counter == 2)
				op_states = C;
		}
		else if (op_states == G) {
			ch -> getval_gpio(ch_value);

			if (ch_value == CH_TRUE)
				op_states = H;
			else
				op_states = L;
		}
		else {
			cout << "Not good" << endl;
		}

	}

	gpio_unexport_process();
	return 0;
}


/*
quit_signal_handler:
---------------------

    This function terminates the program normally when
    Control-c is pressed (SIGINT unix signal)
*/
void quit_signal_handler(int signum)
{
	printf("\nControl-c pressed (SIGINT), do you really");
	printf(" want to exit? [Y/n]");
	char choise;
	scanf("%c",&choise);
	printf("\n");
	if ((choise == Y_SMALL) || (choise == Y_BIG)){
		gpio_unexport_process();
		turn_on_off_gpio("Exiting", OFF, sw_fc);
		turn_on_off_gpio("Exiting", OFF, sw_sc_c);
		turn_on_off_gpio("Exiting", OFF, sw_sc_d);
		printf("TRBS automaton finished !!!%d\n",signum);
		exit(signum);
	}
	return;
}


/*
gpio_init_process:
-------------------

	This function initializes gpios. Exporting, setting
	direction etc.
*/
void gpio_init_process()
{
	// Creating new instances of the gpio_class for gpios
	sw_fc = new gpio_class(SW_FC_GPIO);
	sw_sc_c = new gpio_class(SW_SC_C_GPIO);
	sw_sc_d = new gpio_class(SW_SC_D_GPIO);
	t = new gpio_class(T_GPIO);
	ch = new gpio_class(CH_GPIO);

	// Exporting gpios
	sw_fc -> export_gpio();
	sw_sc_c -> export_gpio();
	sw_sc_d -> export_gpio();
	t -> export_gpio();
	ch -> export_gpio();

	// Setting the gpios direction
	sw_fc -> setdir_gpio("out");
	sw_sc_c -> setdir_gpio("out");
	sw_sc_d -> setdir_gpio("out");
	t -> setdir_gpio("in");
	ch -> setdir_gpio("in");
}



/*
gpio_unexport_process:
-----------------------

	This function is unexporting gpios when it is needed.
*/
void gpio_unexport_process()
{
	// Unexporting gpios
	sw_fc -> unexport_gpio();
	sw_sc_c -> unexport_gpio();
	sw_sc_d -> unexport_gpio();
	t -> unexport_gpio();
	ch -> unexport_gpio();
}


/*
turn_on_off_gpio:
------------------

	This function changes the status of a gpio when it is
	called. It also checks if the status has really changed,
	and in this case returns true. Otherwhise, it returns
	false.
*/
int turn_on_off_gpio(string state, string status, void* gpio)
{
	cout << "\t" << state << endl;
	int return_status;
	string gpio_value;
	gpio_class* gpio_aux = (gpio_class*)gpio;
	gpio_aux -> setval_gpio(status);
	usleep(SEC_TIME);  // waiting for a while
	gpio_aux -> getval_gpio(gpio_value);

	if (gpio_value == status) {  // checking gpio value
		return_status = true;
	}
	else {
		return_status = false;
	}
	return return_status;
}



/*
automaton_init_op:
-------------------

	This function is called to initialize the automaton.
	It tries to turn off all the gpios with direction "out".
*/
void automaton_init_op()
{
	string gpio_value; // auxiliary var to load gpio's value
	int status;
	cout << "Initializing operation" << endl;

	switch (op_states) {
		case P:
			// trying to turn off sw_fc gpio
			status = turn_on_off_gpio("P", OFF, sw_fc);

			if (status == true) {  // checking gpio value
				op_states = Q;  // change state
			}
			else {
				op_states = P;  // stay in the same state
			}
			break;

		case Q:
			// trying to turn off sw_sc_c gpio
			status = turn_on_off_gpio("Q", OFF, sw_sc_c);

			if (status == true) {  // checking gpio value
				op_states = R;  // change state
			}
			else {
				op_states = Q;  // stay in the same state
			}
			break;

		case R:
			// trying to turn off sw_sc_d gpio
			status = turn_on_off_gpio("R", OFF, sw_sc_d);

			if (status == true) {  // checking gpio value
				op_states = A;  // change state
			}
			else
				op_states = R;  // stay in the same state
			break;
	}
}



/*
automaton_charging_op:
-----------------------

	This function is called when the system is going to
	charge supercapacitors.
*/
void automaton_charging_op()
{
	string t_value;
	string ch_value;
	int status;

	cout << "Charging operation" << endl;


	switch (op_states) {
		case C:
			// trying to turn off sw_fc gpio
			status = turn_on_off_gpio("C", OFF, sw_fc);

			if (status == true) // checking gpio value
				op_states = D;  // change state
			else
				op_states = C;  // stay in the same state
			break;

		case D:
			// trying to turn off sw_sc_d gpio
			status = turn_on_off_gpio("D", OFF, sw_sc_d);

			if (status == true) // checking gpio value
				op_states = E;  // change state
			else
				op_states = D;  // stay in the same state
			break;

		case E:
			// trying to turn on sw_sc_c gpio
			status = turn_on_off_gpio("E", ON, sw_sc_c);

			if (status == true)  // checking gpio value
				op_states = F;  // change state
			else
				op_states = E;  // stay in the same state
			break;

		case F:

			cout << "\tF" << endl;
			usleep(SEC_TIME);
			t -> getval_gpio(t_value);
			ch -> getval_gpio(ch_value);

			if ((t_value == T_PRESSED) || (ch_value == CH_TRUE))
				op_states = A; // change state
			else
				op_states = F;  // stay in the same state
			break;
	}
}



/*
automaton_discharging_op:
--------------------------

	This function is called when the system "decides" to
	move the car by using supercapacitors.
*/
void automaton_discharging_op()
{
	string t_value;
	string ch_value;
	int status;

	cout << "Discharging operation" << endl;


	switch (op_states) {
		case H:
			// trying to turn off sw_fc gpio
			status = turn_on_off_gpio("H", OFF, sw_fc);

			if (status == true) // checking gpio value
				op_states = I;  // change state
			else
				op_states = H;  // stay in the same state
			break;

		case I:
			// trying to turn off sw_sc_c gpio
			status = turn_on_off_gpio("I", OFF, sw_sc_c);

			if (status == true) // checking gpio value
				op_states = J;  // change state
			else
				op_states = I;  // stay in the same state
			break;

		case J:
			// trying to turn on sw_sc_d gpio
			status = turn_on_off_gpio("J", ON, sw_sc_d);

			if (status == true) // checking gpio value
				op_states = K;  // change state
			else
				op_states = J;  // stay in the same state
			break;

		case K:

			usleep(SEC_TIME);
			t -> getval_gpio(t_value);
			ch -> getval_gpio(ch_value);

			if ((t_value == T_UNPRESSED) || (ch_value == CH_FALSE))
				op_states = A;  // change state
			else
				op_states = K;  // stay in the same state
			break;
	}
}


void automaton_fc_op()
{
	string t_value;
	string ch_value;
	int status;

	cout << "FC operation" << endl;


	switch (op_states) {
		case L:
			status = turn_on_off_gpio("L", OFF, sw_sc_c);

			if (status == true) // checking gpio value
				op_states = M;
			else
				op_states = L;
			break;

		case M:
			status = turn_on_off_gpio("M", OFF, sw_sc_d);

			if (status == true) // checking gpio value
				op_states = N;
			else
				op_states = M;
			break;

		case N:
			status = turn_on_off_gpio("N", ON, sw_fc);

			if (status == true)// checking gpio value
				op_states = O;
			else
				op_states = N;
			break;

		case O:

			usleep(SEC_TIME);
			t -> getval_gpio(t_value);
			ch -> getval_gpio(ch_value);

			if ((t_value == T_UNPRESSED))
				op_states = A;
			else
				op_states = O;
			break;
	}
}



void print_logo()
{
	cout << "\n\t+-----------------------+" << endl;
	cout << "\t| TUCER Electronics Lab |" << endl;
	cout << "\t|       July 2014       |" << endl;
	cout << "\t|=======================|" << endl;
	cout << "\t|                       |" << endl;
	cout << "\t|    TRBS automaton     |" << endl;
	cout << "\t+-----------------------+\n" << endl;
}
