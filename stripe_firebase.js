/*
 * Project: Doro
 * Author: YianisTz
 * Description: In this file, a simple interaction between Firebase database
 * and Stripe API, using Firebase functions (Nodejs), is presented.
 * #Firebase #Stripe #API #order #promises
 *
 * Created on Sep 15, 2018
 */

const functions = require('firebase-functions');
const admin = require('firebase-admin');
var stripe = require("stripe")("sk_test_z000000000000000000000000");
const request = require('request');

admin.initializeApp();

exports.stripeCharge = functions.database.ref('/orders/{pushId}')
    .onCreate((snapshot, context) => {
      const val = snapshot.val();
      return admin.database().ref('/providers/'+val.providerId+'/stripe_id')
          .once('value').then((snap) => {
            // Get the provider's Stripe id
            return snap.val();
          }).then((providerStripeId) => {

            // Prepare the charge object
            const amount = val.amount;
            const provider_stripe_id = providerStripeId;
            const currency =  "usd";
            const source = val.cardToken;
            const charge = {amount, currency, source};

            // Execute the charge using Stripe API
            return stripe.charges.create(charge, {stripe_account: provider_stripe_id});
          }).then((response) => {
            // The charge was executed successfully
            // Save Stripe response (charge details) in the database
            return snapshot.ref.child('response').set(response);
          }).catch((error) => {
            console.log(error.message);
          });
    });
