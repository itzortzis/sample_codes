 /*
  * File: encoding.c
  * Author: YianisTz
  * Description: Part of a web crawler
  * #webcrawler #lucene #indexing #links
  *
  * Created on April 10, 2015
  */

package crawlerProject;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.util.Scanner;
//import java.text.Format.Field;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.*;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.Searcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author john
 */
public class Indexing {
  final String INDEX_PATH = "crawlerProject/indexing";
  final String TEST_FILE = "crawlerProject/test_folder/27.html.txt";
  final String TXT_FILES = "crawlerProject/final_txt_files";
  final String FORWARD_INDEX = "crawlerProject/forward_index";
  final String BACKWARD_INDEX = "crawlerProject/backward_index/";
  final int NUM_OF_SEARCH_RESULTS = 10;
  private StandardAnalyzer analyzer;

  public Indexing() throws IOException {
    File newFile = new File(TEST_FILE);
    System.out.println(getFileContent(newFile));
    StandardAnalyzer analyzer;

    File indexDest = new File(INDEX_PATH);
    File dataDir = new File(TXT_FILES);
    initIndexing(indexDest, dataDir);
  }


  /*
   * Initializing indexing.
   * Setting data directory and index destination
   */
  public void initIndexing(File indexDir, File dataDir)
                           throws IOException {
    analyzer = new StandardAnalyzer();
    try {
      IndexWriter writer = new IndexWriter(indexDir,
                   analyzer, true);
      dirIndexing(writer, dataDir);
      writer.close();
    }
    catch (IOException e) {
      System.out.println(dataDir.getName()+
                        " does not exist or is not directory.");
    }
  }


  /*
   * Indexing on all files of a folder
   */
  public void dirIndexing(IndexWriter writer, File folder)
                           throws IOException {

    for (File fileEntry : folder.listFiles()) {
      if (fileEntry.isDirectory()) {
        dirIndexing(writer, fileEntry);
      }
      else {
        fileIndexing(writer, fileEntry);
      }
    }
  }

  /*
   * Indexing on a specific file
   */
  public void fileIndexing(IndexWriter writer, File fileEntry)
                            throws IOException {
    System.out.println("Indexing " + fileEntry.getName());

    Document doc = new Document();
    String path = fileEntry.getName();
    doc.add(new Field("path", path, Field.Store.YES,
                      Field.Index.ANALYZED));
    Reader reader = new FileReader(fileEntry);
	    doc.add(new Field("contents", reader,Field.TermVector.YES));
    writer.addDocument(doc);
  }

  /*
   * Using a query to search in the indexed files
   */
  public void search(String queryString)  throws Exception{
    Searcher searcher = new IndexSearcher
                   (FSDirectory.getDirectory(INDEX_PATH));
    QueryParser parser = new QueryParser("contents", new StandardAnalyzer());

    System.out.println("Index searching for: " + queryString);
    Query query = parser.parse(queryString);
    TopDocs results = searcher.search(query, NUM_OF_SEARCH_RESULTS);
    System.out.println("total hits: " + results.totalHits);
    ScoreDoc[] hits = results.scoreDocs;
    for (ScoreDoc hit : hits) {
      Document doc = searcher.doc(hit.doc);
      System.out.printf("%5.3f %s", hit.score, doc.get("path"));
      System.out.println();
    }
  }


  /*
   * The following method is an auxiliary method for backwards indexing
   */
  public void backwardsIndexAux(String queryString, String filename)
                              throws Exception{
    Searcher searcher = new IndexSearcher
                   (FSDirectory.getDirectory(INDEX_PATH));
    QueryParser parser = new QueryParser("contents", new StandardAnalyzer());

    //System.out.println("Index searching for: " + queryString);
    Query query = parser.parse(queryString);
    TopDocs results = searcher.search(query, NUM_OF_SEARCH_RESULTS);
    //System.out.println("total hits: " + results.totalHits);
    ScoreDoc[] hits = results.scoreDocs;

    BufferedWriter writer = new BufferedWriter(new FileWriter
                    (BACKWARD_INDEX+filename+"backward.txt"));
    System.out.println("Creating backward index for file: "+filename);
    for (ScoreDoc hit : hits) {
      Document doc = searcher.doc(hit.doc);
      writer.write(doc.get("path"));
      writer.newLine();
    }
    writer.close();
  }
}
