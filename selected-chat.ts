/*
 * File: encoding.c
 * Author: YianisTz
 * Description: Chat client 
 * #Typescript #Firebase #Chat #Json #Pagination
 *
 * Created on Jan 10, 2020
 */

import { Component, OnInit, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import * as firebase from 'firebase';
import { NavParamsService } from '../../services/navParams.service';

@Component({
  selector: 'app-selected-chat',
  templateUrl: './selected-chat.page.html',
  styleUrls: ['./selected-chat.page.scss'],
})
export class SelectedChatPage implements OnInit {

  userId: string;
  modelName: string;

  // Chat variables
  partenerName: string;
  newMsg: string;
  myChatsRef: any;
  chatId: string;
  messages = [];

  // Pagination variables
  pageSize: number;
  noOlderData: boolean;
  lastKey: any;
  lastValue: any;
  severalCreations: boolean;

  // Database references
  msgRef: any;
  chatRef: any;
  usersRef: any;

  @ViewChild('pageTop', { static: false }) private pageTop: any;

  constructor(
    private params: NavParamsService
  ) {
    this.initChatDetails();
    this.initPaginationVars();
    this.createDbReferences();
    this.fetchChatDetails();
    this.fetchChatMessages();
  }

  ionViewDidEnter() {
    this.pageScroller();
  }

  initChatDetails() {
    this.userId = this.params.getLoggedUserId();
    this.chatId = this.params.getChatId();
    this.modelName = "";
    this.partenerName = "";
    this.newMsg = "";
  }

  initPaginationVars() {
    this.severalCreations = false;
    this.noOlderData = false;
    this.lastKey = null;
    this.lastValue = null;
    this.pageSize = 5;
  }

  pageScroller() {
    let rootRegion = this;
    setTimeout(function() {
      rootRegion.pageTop.scrollToBottom(300);
    }, 500);

  }

  ngOnInit() {

  }


  createDbReferences() {
    // Initialize database references
    this.myChatsRef = firebase.database()
        .ref('/chats/'+this.chatId+'/messages').orderByChild("date");
    this.msgRef = firebase.database().ref('/chats/'+this.chatId+'/messages');
    this.chatRef = firebase.database().ref('/chats/'+this.chatId+'/');
    this.usersRef = firebase.database().ref('/users');
  }

  fetchChatDetails() {
    // Fetch chat details
    this.chatRef.once('value', details => {
      var id;
      if (this.userId == details.val().providerId)
        id = details.val().clientId;
      else
        id = details.val().providerId;

      // Fetch partner name
      this.usersRef.child(id+'/').once('value', user => {
        this.partenerName = user.val().username;
        console.log(this.partenerName);
      })
    });
  }

  sendMsg() {
    this.messages = [];
    // Push new message object
    this.msgRef.push({
      "date": -Date.now(),
      "text": this.newMsg,
      "author": this.userId,
      "img": null
    });
    this.newMsg = "";
    // Refresh chat messages
    this.fetchChatMessages();
    this.pageScroller();
  }

  fetchChatMessages() {

    var firstPage = true;
    let ref = this.myChatsRef;
    if( this.lastValue !== null ) {
      firstPage = false;
      // Set reference to the current message
      ref = ref.startAt(this.lastValue, this.lastKey).limitToFirst(this.pageSize+1);
    } else {
      firstPage = true;
      // Set reference to the first message
      ref = ref.limitToFirst(this.pageSize);
    }
    ref.on('value', messages => {
      const keys = [];
      const data = []; // store data in array so it's ordered
      var skipMarginalCreation = true;
      messages.forEach(message => {
        var msg = this.createArrayTemplate(message);

        data.push(msg);
        keys.push(message.key);

        if (!skipMarginalCreation)
            this.messages.unshift(msg);
        else {
          if (firstPage)
              this.messages.unshift(msg);
          else
            skipMarginalCreation = false;
        }
        if(this.messages.length > this.pageSize)
          this.severalCreations = true;
      });


      if( this.lastValue !== null ) {
        // skip the first value, which is actually the cursor
        //this.messages.shift();
        keys.shift();
        data.shift();
      }

      // store the last loaded record
      if( data.length ) {
        const last = data.length - 1;
        this.lastKey = keys[last];
        this.lastValue = -(data[last].msgDate);
      }
      if (data.length == 0) {
        this.noOlderData = true;
      }

     });
  }

  createArrayTemplate(message: any) {
    var msg;
    if (this.userId == message.val().author) {
      msg = {
          "msgId": message.key,
          "msgDate": -message.val().date,
          "msgAuthor": message.val().author,
          "msgAuthorName": "Me",
          "msgText": message.val().text,
          "msgImg": message.val().img
      };
    } else {
      msg = {
          "msgId": message.key,
          "msgDate": -message.val().date,
          "msgAuthor": message.val().author,
          "msgAuthorName": this.partenerName,
          "msgText": message.val().text,
          "msgImg": message.val().img
      };
    }
    console.log(message.val());
    return msg;
  }

}
